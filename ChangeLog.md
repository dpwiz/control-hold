# Changelog for control-hold

## 0.1.2.1

* Put traces under `--trace` flag.

## 0.1.2.0

* Changed default port to 8949.

## 0.1.1.0

* Added Korg NanoKontrol2 parameter mapping

## 0.1.0.0

Initial import
