module Sound.ControlHold.Types where

import Data.Text (Text)
import Data.Word (Word8, Word32)
import Data.Int (Int32)

data Liveness
  = Stored
  | Live
  deriving (Eq, Ord, Show, Enum, Bounded)

type Key = (Text, Text, Word8, Word32)

type Value = (Liveness, Int32)

defaultPort :: Int
defaultPort = 8949
