module Sound.ControlHold.Storage where

import Debug.Trace (traceShowM)
import System.Directory (XdgDirectory(..), doesFileExist, getXdgDirectory)
import System.FilePath ((</>))

import qualified Data.Text as Text
import qualified Data.Text.IO as TextIO
import qualified Data.Text.Read as TextRead

import Sound.ControlHold.Types

loadValues :: FilePath -> IO [(Key, Value)]
loadValues storageFile = do
  exists <- doesFileExist storageFile
  if exists then do
    old <- TextIO.readFile storageFile
    pure do
      line <- Text.lines old
      case parseItem line of
        Left err -> do
          traceShowM err
          []
        Right item ->
          pure item
  else
    pure []

getValuesPath :: IO FilePath
getValuesPath =
  fmap (</> "values.txt") getStorageDirectory

getStorageDirectory :: IO FilePath
getStorageDirectory =
  getXdgDirectory XdgCache "control-hold"

parseItem :: Text.Text -> Either String (Key, Value)
parseItem line =
  case Text.split (== '\t') line of
    [client, port, channel, parameter, liveness, value'] -> do
      let
        keyP = (,,,)
          <$> pure client
          <*> pure port
          <*> fmap fst (TextRead.decimal channel)
          <*> fmap fst (TextRead.signed TextRead.decimal parameter)

        valueP = (,)
          <$> livenessP liveness
          <*> fmap fst (TextRead.signed TextRead.decimal value')

        livenessP = \case
          "Live" ->
            pure Live
          "Stored" ->
            pure Stored
          unexpected ->
            Left $ "unexpected liveness: " <> show unexpected

      case (,) <$> keyP <*> valueP of
        Right (key, value) ->
          pure (key, value)
        Left err -> do
          Left err
    unexpected -> do
      Left $ show unexpected
