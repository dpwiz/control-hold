{-# LANGUAGE DeriveFunctor #-}

module Sound.ControlHold.Client.NanoKontrol2
  ( mkHandler
  , defaultParameterMap

  , mkHandler_
  , SimpleValueMap(..)
  , Balanced(..)

  , Event(..)
  , Function(..)
  , Group(..)
  ) where

import Data.Int (Int32)
import Data.Word (Word32)

import Sound.ControlHold.Client (Handler)
import Sound.ControlHold.Types (Key, Value)

mkHandler
  :: (Int32 -> Maybe a)
  -> ((Key, Value) -> IO ())
  -> (Event a -> IO ())
  -> Handler
mkHandler valueMap unknown action key value =
  case defaultParameterMap valueMap key value of
    Right event ->
      action event
    Left item ->
      unknown item

data Event a
  = Function Function Bool
  | Group Word32 (Group a)
  deriving (Eq, Ord, Show, Functor)

data Function
  = TrackLeft
  | TrackRight
  | Cycle
  | MarkerSet
  | MarkerLeft
  | MarkerRight
  | Rewind
  | FastForward
  | Stop
  | Play
  | Record
  deriving (Eq, Ord, Show)

data Group a
  = Knob   a
  | Slider a
  | Solo   Bool
  | Mute   Bool
  | Rec    Bool
  deriving (Eq, Ord, Show, Functor)

defaultParameterMap
  :: (Int32 -> Maybe a)
  -> Key
  -> Value
  -> Either (Key, Value) (Event a)
defaultParameterMap valueMap key value = result
  where
    result
      | parameter `elem` [0..7] =
          withValue $ Group parameter . Slider
      | parameter `elem` [16..23] =
          withValue $ Group (parameter - 16) . Knob
      | parameter `elem` [32..39] =
          Right $ Group (parameter - 32) (Solo valueB)
      | parameter `elem` [48..55] =
          Right $ Group (parameter - 48) (Mute valueB)
      | parameter `elem` [64..71] =
          Right $ Group (parameter - 64) (Rec valueB)
      | otherwise =
          case parameter of
            58 -> trackButton TrackLeft
            59 -> trackButton TrackRight

            46 -> trackButton Cycle

            60 -> trackButton MarkerSet
            61 -> trackButton MarkerLeft
            62 -> trackButton MarkerRight

            43 -> trackButton Rewind
            44 -> trackButton FastForward
            42 -> trackButton Stop
            41 -> trackButton Play
            45 -> trackButton Record
            _ ->
              unknown

    (_client, _port, _channel, parameter) = key
    (_liveness, valueI) = value
    unknown = Left (key, value)

    withValue f =
      case valueMap valueI of
        Nothing ->
          unknown
        Just x ->
          Right $ f x

    valueB = valueI /= 0
    trackButton btn = Right $ Function btn valueB

-- * Convenience

mkHandler_ :: SimpleValueMap a => (Event a -> IO ()) -> Handler
mkHandler_ = mkHandler simpleValueMap (\_ignore -> pure ())

class SimpleValueMap a where
  simpleValueMap :: Int32 -> Maybe a

instance SimpleValueMap Int32 where
  {-# INLINE simpleValueMap #-}
  simpleValueMap =
    Just

instance SimpleValueMap Int where
  {-# INLINE simpleValueMap #-}
  simpleValueMap =
    Just . fromIntegral

instance SimpleValueMap Word where
  {-# INLINE simpleValueMap #-}
  simpleValueMap x =
    if x < 0 then
      Nothing
    else
      Just $ fromIntegral x

instance SimpleValueMap Float where
  {-# INLINE simpleValueMap #-}
  simpleValueMap i
    | i < 0 = Nothing
    | i > 127 = Nothing
    | otherwise = Just (fromIntegral i / 127.0) -- XXX: there's no 0.5

instance SimpleValueMap Double where
  {-# INLINE simpleValueMap #-}
  simpleValueMap i
    | i < 0 = Nothing
    | i > 127 = Nothing
    | otherwise = Just (fromIntegral i / 127.0) -- XXX: there's no 0.5

newtype Balanced a = Balanced { getBalanced :: a }
  deriving (Eq, Ord, Show, Functor, Num)

instance Fractional a => SimpleValueMap (Balanced a) where
  simpleValueMap i
    | i < 0   = Nothing
    | i > 127 = Nothing
    | i < 64  = Just . Balanced $ fromIntegral (i - 64) / 128
    | i > 64  = Just . Balanced $ fromIntegral (i - 63) / 128
    | otherwise = Just $ Balanced 0 -- XXX: i == 64, but without "incomplete patterns" warning
