module Sound.ControlHold.Client where

import Control.Monad (forever)
import Debug.Trace (traceShowM)

import qualified Network.WebSockets as WS

import Sound.ControlHold.Types (Key, Value, defaultPort)

import qualified Sound.ControlHold.Storage as Storage

type Handler = Key -> Value -> IO ()

runDefault :: Handler -> IO ()
runDefault = run "127.0.0.1" defaultPort

run :: String -> Int -> Handler -> IO ()
run host port handler =
  WS.runClient host port "/" \connection ->
    WS.withPingThread connection 30 (pure ()) $
      app handler connection

app :: Handler -> WS.ClientApp ()
app handler connection =
  forever do
    line <- WS.receiveData connection
    case Storage.parseItem line of
      Left err -> do
        traceShowM err
        pure ()
      Right (key, value) ->
        handler key value
