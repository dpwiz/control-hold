module Main where

import Control.Concurrent (forkIO, threadDelay)
import Control.Monad
import Data.Foldable
import Data.IORef (IORef, newIORef, writeIORef, readIORef)
import System.Directory (createDirectoryIfMissing)
import System.Environment (getArgs)
import System.FilePath (takeDirectory)
import System.IO.Unsafe (unsafePerformIO)

import qualified Control.Concurrent.STM as STM
import qualified Data.EnumBitSet as EnumBitSet
import qualified Data.List as List
import qualified Data.Text as Text
import qualified Data.Text.IO as TextIO
import qualified Debug.Trace as Trace
import qualified ListT
import qualified Network.WebSockets as WS
import qualified Sound.ALSA.Sequencer as Sequencer
import qualified Sound.ALSA.Sequencer.Address as Address
import qualified Sound.ALSA.Sequencer.Client as Client
import qualified Sound.ALSA.Sequencer.Client.Info as ClientInfo
import qualified Sound.ALSA.Sequencer.Event as Event
import qualified Sound.ALSA.Sequencer.Port as Port
import qualified Sound.ALSA.Sequencer.Port.Info as PortInfo
import qualified Sound.ALSA.Sequencer.Subscribe as Subscribe
import qualified StmContainers.Bimap as Bimap
import qualified StmContainers.Map as Map
import qualified UnliftIO

import Sound.ControlHold.Types

import qualified Sound.ControlHold.Storage as Storage

main :: IO ()
main = do
  setupTrace

  currentClients <- Bimap.newIO
  dirty <- STM.newTVarIO False

  currentValues <- Map.newIO
  valuesFile <- Storage.getValuesPath
  createDirectoryIfMissing True (takeDirectory valuesFile)
  Storage.loadValues valuesFile >>= \items ->
    STM.atomically $ for_ items \(key, value) ->
      Map.insert value key currentValues

  me <- Sequencer.openDefault @Sequencer.InputMode Sequencer.Block
  myInfo <- ClientInfo.get me
  myClient <- ClientInfo.getClient myInfo

  myPort <- Port.createSimple me "input" myCaps Port.typeApplication
  myPortInfo <- PortInfo.get me myPort
  myPortAddr <- PortInfo.getAddr myPortInfo

  clients <- listClients me
  let
    interesting = do
      (clientName, clientPorts, client) <- clients
      guard $ client /= myClient
      (portName, clientAddr, caps, chans, _clientPort) <- clientPorts
      guard $ chans > 0
      pure
        ( Text.pack clientName
        , Text.pack portName
        , caps
        , clientAddr
        )

  for_ interesting \(clientName, portName, caps, clientAddr) -> do
    let
      Address.Cons{client, port} = clientAddr
      Client.Cons clientId = client
      Port.Cons portId = port
      portKey = (clientId, portId)

    STM.atomically $
      Bimap.insertLeft portKey (clientName, portName) currentClients

    putStrLn $ mconcat
      [ show clientId, ":", show portId
      , "\t"
      , Text.unpack clientName
      , "\t"
      , Text.unpack portName
      ]
    putStrLn $ "  " <> show (EnumBitSet.toEnums caps)
    Subscribe.subscribe me clientAddr myPortAddr False Nothing

  _storagePid <- forkIO $ forever do
    changed <- STM.atomically do
      isDirty <- STM.readTVar dirty
      if isDirty then do
        STM.writeTVar dirty False
        items <- ListT.toList $ Map.listT currentValues
        pure items
      else
        STM.retry

    TextIO.writeFile valuesFile $ Text.unlines do
      (key, (_live, value)) <- changed
      traceShowM (key, value)
      let (client, port, channel, parameter) = key
      pure $ Text.intercalate "\t"
        [ client
        , port
        , Text.pack $ show channel
        , Text.pack $ show parameter
        , Text.pack $ show Stored
        , Text.pack $ show value
        ]

    threadDelay 1000000

  publish <- STM.newBroadcastTChanIO

  _serverPid <- forkIO $ WS.runServer "127.0.0.1" defaultPort \pending -> do
    connection <- WS.acceptRequest pending
    subscribe <- STM.atomically $ STM.dupTChan publish

    items <- STM.atomically . ListT.toList $ Map.listT currentValues
    -- TODO: send subscribers' menu
    -- WS.sendTextDatas connection $ List.nub do
    --   (client, port, _channel, _control) <- map fst items
    --   pure $ mconcat
    --     [ client
    --     , "\t"
    --     , port
    --     ]
    WS.sendTextDatas connection do
      (key, value') <- items
      let
        (sourceClient, sourcePort, channel, parameter) = key
        (liveness, value) = value'

      pure $ Text.intercalate "\t"
        [ sourceClient
        , sourcePort
        , Text.pack $ show channel
        , Text.pack $ show parameter
        , Text.pack $ show liveness
        , Text.pack $ show value
        ]

    let
      sender = forever do
        item <- STM.atomically $ STM.readTChan subscribe
        WS.sendTextData connection item

      receiver = forever do
        msg <- WS.receiveData connection
        case Text.split (== '\t') msg of
          "SUB" : client : port : channel ->
            -- TODO: dump stored state and subscribe
            traceShowM ("SUB", client, port, channel)
          unknown ->
            traceShowM unknown

    UnliftIO.race_ sender receiver

  forever $
    Event.input me >>= \Event.Cons{source, body} ->
      STM.atomically do
        let
          Address.Cons{client, port} = source
          Client.Cons clientId = client
          Port.Cons portId = port
          sourceKey = (clientId, portId)
        Bimap.lookupRight sourceKey currentClients >>= \case
          Nothing ->
            case body of
              Event.ConnEv Event.PortSubscribed connection ->
                -- XXX: external connect
                traceM $ show connection
              Event.ConnEv Event.PortUnsubscribed connection ->
                -- XXX: device unplugged or external disconnect
                traceM $ show connection
              _ ->
                traceM $ "Unexpected event from unknown client: " <> show body

          Just (sourceClient, sourcePort) ->
            case body of
              Event.CtrlEv Event.Controller Event.Ctrl{..} -> do
                let
                  channel = Event.unChannel ctrlChannel
                  parameter = Event.unParameter ctrlParam
                  value = Event.unValue ctrlValue

                  valueKey = (sourceClient, sourcePort, channel, parameter)

                traceM $ List.intercalate "\t"
                  [ Text.unpack sourceClient
                  , Text.unpack sourcePort
                  , show channel
                  , show parameter
                  , show value
                  ]
                Map.insert (Live, value) valueKey currentValues
                STM.writeTVar dirty True
                STM.writeTChan publish $
                  Text.intercalate "\t"
                    [ sourceClient
                    , sourcePort
                    , Text.pack $ show channel
                    , Text.pack $ show parameter
                    , Text.pack $ show Live
                    , Text.pack $ show value
                    ]
              _others ->
                error $ "TODO: other events from known clients: " <> show body

      {- Disconnect:

      ( Address.Cons (Client.Cons 0) (Port.Cons 1)
      , ConnEv
          PortUnsubscribed
          ( Cons
              { source =
                  Address.Cons (Client.Cons 28) (Port.Cons 0)
              , dest =
                  Address.Cons (Client.Cons 128) (Port.Cons 0)
              }
          )
      )
      -}

myCaps :: Port.Cap
myCaps = Port.caps
  [ Port.capWrite
  , Port.capSubsWrite
  ]

type Client = (String, [Port], Client.T)

listClients :: Sequencer.T mode -> IO [Client]
listClients sequencer =
  ClientInfo.queryLoop sequencer \info -> do
    name <- ClientInfo.getName info
    client <- ClientInfo.getClient info
    ports <- listPorts sequencer client
    pure (name, ports, client)

type Port = (String, Address.T, Port.Cap, Word, Port.T)

listPorts
  :: Sequencer.T mode
  -> Client.T
  -> IO [Port]
listPorts sequencer client =
  PortInfo.queryLoop sequencer client \info -> (,,,,)
    <$> PortInfo.getName info
    <*> PortInfo.getAddr info
    <*> PortInfo.getCapability info
    <*> PortInfo.getMidiChannels info
    <*> PortInfo.getPort info

setupTrace :: IO ()
setupTrace = do
  args <- getArgs
  when (elem "--trace" args) $
    writeIORef traceEnabled True

traceM :: Monad m => String -> m ()
traceM =
  when (unsafePerformIO $ readIORef traceEnabled) . Trace.traceM

traceShowM :: (Show a, Monad m) => a -> m ()
traceShowM =
  when (unsafePerformIO $ readIORef traceEnabled) . Trace.traceShowM

{-# NOINLINE traceEnabled #-}
traceEnabled :: IORef Bool
traceEnabled = unsafePerformIO (newIORef False)
