# 🎛 control-hold

A service to hold last known values for `Control Change` events.

> Your hardware has this fine property of persistence.
> That is, if you tweak some control it *stays tweaked*, even after reboot.

Alas, General MIDI has nothing in its protocol to query control values.
So this little service attempts to recover this property in a general way: anyone can read the control values without resorting to proprietaty SysEx messages (not all devices have them in to begin with).

It consists of `control-hold` executable to be autostarted in user session and a `Sound.ControlHold` library to connect.

The protocol is, for now, a tab-separated text messages inside a websocket transport.

> Yes, the latency is noticeable, but that's not the point.
> Use [PortMidi-simple] or [alsa-seq] directly if you need to go faster.

[PortMidi-simple]: https://gitlab.com/dpwiz/PortMidi-simple
[alsa-seq]: https://hackage.haskell.org/package/alsa-seq

It uses port `8949` by default and nothing is configurable (patches welcome).

## The Service

Build, install executable, add a startup script.

Systemd users can put something like this under `~/.config/systemd/user/control-hold.service`:

```ini
[Unit]
Description=Service to hold MIDI Control Change values

[Service]
ExecStart=%h/.local/bin/control-hold
Restart=always
RestartSec=60

[Install]
WantedBy=default.target
```

## The Library

```haskell
import Sound.ControlHold.Client (runDefault)

main = runDefault \key value -> print (key, value)
```

You'll get the dump of all known values and the stream will segue into live update mode:

```haskell
(("nanoKONTROL2","nanoKONTROL2 MIDI 1",0,20),(Stored,63))
(("nanoKONTROL2","nanoKONTROL2 MIDI 1",0,21),(Stored,63))
(("nanoKONTROL2","nanoKONTROL2 MIDI 1",0,22),(Stored,63))
(("nanoKONTROL2","nanoKONTROL2 MIDI 1",0,0),(Live,108))
(("nanoKONTROL2","nanoKONTROL2 MIDI 1",0,0),(Live,107))
(("nanoKONTROL2","nanoKONTROL2 MIDI 1",0,0),(Live,106))
(("nanoKONTROL2","nanoKONTROL2 MIDI 1",0,0),(Live,105))
(("nanoKONTROL2","nanoKONTROL2 MIDI 1",0,0),(Live,104))
```

## The Protocol

- Q: Why TSV over websocket?
- A:
    ```js
    ws = new WebSocket('ws://127.0.0.1:8949')
    ws.onmessage = (msg) => console.debug(msg.data.split(/t/))
    > ["nanoKONTROL2", "nanoKONTROL2 MIDI 1", "0", "0", "Live", "101"]
    ```

The items being:

* Client (device) name
* Port name
* Channel number
* Parameter number
* `Live` or `Stored` flag
* Value number
